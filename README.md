# Docker images for Gimp CI

## How to push images
If you are not already logged in, you need to authenticate to the Container
Registry by using your GitLab username and password.
If you have Two-Factor Authentication enabled, use a Personal Access Token instead of a password.

```
docker login registry.gitlab.gnome.org
```

You can add an image to this registry with the following commands:

```
docker build -t registry.gitlab.gnome.org/gnome/gimp/archlinux .
docker push registry.gitlab.gnome.org/gnome/gimp/archlinux
```
